#include "TaskInitialization.hpp"

void initializeTasks() {
    SEGGER_RTT_Init();
    BootCounter::incrementBootCounter();

    uartGatekeeperTask.emplace();
    timeKeepingTask.emplace();
    watchdogTask.emplace();
    mcuTemperatureTask.emplace();
    canGatekeeperTask.emplace();
    tcHandlingTask.emplace();

    mcuTemperatureTask->createTask();
    timeKeepingTask->createTask();
    uartGatekeeperTask->createTask();
    watchdogTask->createTask();
    canGatekeeperTask->createTask();
    tcHandlingTask->createTask();
}

void initializeAmbientTemperatureTask() {
    ambientTemperatureTask.emplace();
    ambientTemperatureTask->createTask();
}

void resetChecks() {
#ifdef OBC_EQM
    RSTC_RESET_CAUSE resetCause = RSTC_ResetCauseGet();
    if(resetCause == RSTC_GENERAL_RESET){
        LOG_DEBUG<<"Last reset was: General reset.";
    }
    else if(resetCause == RSTC_BACKUP_RESET){
        LOG_DEBUG<<"Last reset was: Backup reset.";
    }
    else if(resetCause == RSTC_WATCHDOG_RESET){
        LOG_DEBUG<<"Last reset was: Watchdog reset.";
    }
    else if(resetCause == RSTC_USER_RESET){
        LOG_DEBUG<<"Last reset was: User reset.";
    }
    else if(resetCause == RSTC_SOFTWARE_RESET){
        LOG_DEBUG<<"Last reset was: Software reset.";
    }
    else {
        LOG_DEBUG<<"Last reset was: Undefined reset.";
    }
#endif
}