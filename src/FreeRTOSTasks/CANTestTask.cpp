#include "CANTestTask.hpp"
#include "CANGatekeeperTask.hpp"

void CANTestTask::execute() {
    CAN::Application::switchBus(CAN::Driver::Main);
    switch (CANTestOption) {
        case CANTestOptions::CANSendMessage: {
            /**
             * CAN message sending - WARNING: As of the latest test, this is non-functional
             */
            CAN::Frame message = {};

            /**
             * Simple 64 byte message sending
             */
            message.id = 0x10A;
            for (uint8_t idx = 0; idx < CAN::Frame::MaxDataLength; idx++) {
                message.data.insert(0, idx);
            }

            while (true) {
                canGatekeeperTask->send(message);
                vTaskDelay(pdMS_TO_TICKS(1000));
            }
            break;
        }
        case CANTestOptions::PingMessageOBC:
            /**
             * Ping messages to OBC
             */
            while (true) {
                CAN::Application::sendPingMessage(CAN::NodeIDs::OBC, false);
                vTaskDelay(pdMS_TO_TICKS(1000));
            }
            break;
        case CANTestOptions::HousekeepingParameters:
            /**
             * Housekeeping parameters sending
             */
            while (true) {
                CAN::Application::createSendParametersMessage(CAN::NodeIDs::OBC, false, {2, 7}, false);
                vTaskDelay(pdMS_TO_TICKS(1000));
            }
            break;
        case CANTestOptions::LogMessage:
            /**
             * Log messages utilizing multiple messages
             */
            while (true) {
                const String<ECSSMaxMessageSize> logString = "This is a log message going from the development board to the eqm that consists of more than 64 chars";
                //CAN::Application::createLogMessage(CAN::NodeIDs::OBC, true, logString, false);
                vTaskDelay(pdMS_TO_TICKS(1000));
            }
            break;
    }
}