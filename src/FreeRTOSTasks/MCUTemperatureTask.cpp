#include "MCUTemperatureTask.hpp"
#include "Services/ParameterService.hpp"
#include "AcubeSATParameters.hpp"

MCUTemperatureTask::MCUTemperatureTask(): Task("MCUTemperatureTask") {
    AFEC0_CallbackRegister(
        [](uint32_t status, uintptr_t context) -> void {
            BaseType_t higher_priority_task_woken = pdFALSE;

            if (taskHandle != nullptr) {
                const uint16_t adc_conversion = AFEC0_ChannelResultGet(AFEC_CH11);
                xTaskNotifyFromISR(taskHandle, adc_conversion, eSetValueWithOverwrite, &higher_priority_task_woken);
                portYIELD_FROM_ISR(higher_priority_task_woken);
            } else {
                LOG_ERROR << "ISR called with null task handle!";
            }
        }, reinterpret_cast<uintptr_t>(nullptr)
    );
}

void MCUTemperatureTask::execute() const {
    uint32_t adc_conversion = 0U;

    while (true) {
        AFEC0_ConversionStart();
        xTaskNotifyWait(0U, UINT32_MAX, &adc_conversion, portMAX_DELAY);
        const float voltageConversion = static_cast<float>(adc_conversion) * PositiveVoltageReference / MaxADCValue;
        const float mcuTemperature =
                (voltageConversion - TypicalVoltageAt25) / TemperatureSensitivity + ReferenceTemperature;
        LOG_DEBUG << "The temperature of the MCU is: " << mcuTemperature << " degrees Celsius";
        CommonParameters::mcuTemperature.setValue(mcuTemperature);
        vTaskDelay(pdMS_TO_TICKS(2000));
    }
}