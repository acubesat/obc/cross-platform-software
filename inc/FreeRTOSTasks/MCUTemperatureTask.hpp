#pragma once

#include "Task.hpp"

/**
 * FreeRTOS task for periodically printing the value of the internal temperature sensor.
 */
class MCUTemperatureTask : public Task {
private:
    const uint16_t delayMs = 2000;

    const static inline uint16_t TaskStackDepth = 1000;

    StackType_t taskStack[TaskStackDepth];

    inline static TaskHandle_t taskHandle = nullptr;

public:
    void execute() const;

    MCUTemperatureTask();

    void createTask() {
        taskHandle = xTaskCreateStatic(vClassTask<MCUTemperatureTask>, this->TaskName, TaskStackDepth, this,
                          tskIDLE_PRIORITY + 3, this->taskStack, &(this->taskBuffer));
    }

};

inline std::optional<MCUTemperatureTask> mcuTemperatureTask;
